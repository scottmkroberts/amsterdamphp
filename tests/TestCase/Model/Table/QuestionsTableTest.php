<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QuestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;


/**
 * App\Model\Table\QuestionsTable Test Case
 */
class QuestionsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'Questions' => 'app.questions',
        'Users' => 'app.users',
        'Elections' => 'app.elections',
        'Answers' => 'app.answers',
        'Tags' => 'app.tags',
        'QuestionsTags' => 'app.questions_tags'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Questions') ? [] : ['className' => 'App\Model\Table\QuestionsTable'];
        $this->Questions = TableRegistry::get('Questions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Questions);

        parent::tearDown();
    }

    public function testReassing()
    {
        $saved = $this->Questions->reassign(2,1);
        $this->assertInstanceOf('App\Model\Entity\Question', $saved);
        $this->assertEquals(1, $saved->user_id);
    }

    /**
     * @expectedException \Cake\Datasource\Exception\RecordNotFoundException
     */
    public function testReassingAdminQuestion()
    {
        $saved = $this->Questions->reassign(1,2);
    }

    /**
     * @expectedException \Cake\Network\Exception\NotFoundException
     */
    public function testReassingNotFoundQuestion()
    {
        $saved = $this->Questions->reassign(999,2);
    }

}
