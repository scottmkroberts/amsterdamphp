<div class="row">
    <div class="col-md-4">
        <?= $this->fetch('sidebar'); ?>
    </div>
    <div class="col-md-8">
        <?= $this->fetch('content'); ?>
    </div>
</div>