<?php
namespace App\Model\Table;

use App\Model\Entity\Question;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Network\Exception\NotFoundException;

/**
 * Questions Model
 */
class QuestionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('questions');
        $this->displayField('title');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Elections', [
            'foreignKey' => 'election_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Answers', [
            'foreignKey' => 'question_id'
        ]);
        $this->belongsToMany('Tags', [
            'foreignKey' => 'question_id',
            'targetForeignKey' => 'tag_id',
            'joinTable' => 'questions_tags'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['election_id'], 'Elections'));
        return $rules;
    }

    public function findTag(Query $query, array $options)
    {
        $tag = $options['tag'];
        return $query->matching('Tags', function ($query) use ($tag) {
            return $query->where(['name' => $tag]);
        });
    }

    public function findMystery(Query $query, array $options)
    {
        $query
            ->join([
                'table' => 'answers',
                'alias' => 'a',
                'type' => 'LEFT',
                'conditions' => 'a.question_id = Questions.id',
                ])
            ->where(['a.id IS NULL']);
    }

    /**
     * Reassign a User question to another user
     */
    public function reassign($questionId, $userId)
    {
        $question = $this->find()
            ->where(['Questions.id' => $questionId])
            ->contain(['Users'])
            ->first();
        if (!$question) {
            throw new NotFoundException(__('Question {0} not found', $questionId));
        }
        if ($question->user->role !== UsersTable::ROLE_USER) {
            throw new RecordNotFoundException(__('Question {0} is not a user question', $questionId));
        }
        $question->user_id = $userId;
        return $this->save($question);
    }
}
